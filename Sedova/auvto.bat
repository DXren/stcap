@echo off
echo type "commit" or "update"
cd "C:\Users\TEMP.PD\Documents\Sedova\stcap\Sedova"

set GIT_PATH="C:\Program Files\Git\bin\git.exe"
set BRANCH = "master"

:P
set ACTION=
set /P ACTION=Action: %=%
if "%ACTION%"=="c" (
  %GIT_PATH% git add -A
	%GIT_PATH% git commit -am "Auto-committed on %date%"
	%GIT_PATH% git pull %BRANCH%
	%GIT_PATH% git merge %BRANCH%
	%GIT_PATH% git push %BRANCH%
)
if "%ACTION%"=="u" (
	%GIT_PATH% pull %BRANCH%
)
if "%ACTION%"=="exit" exit /b
goto P